extends Control

const LevelMenu = preload("res://scenes/level_menu.tscn")

func _ready():
	OS.set_window_title('Swarm')
	$chk_sound.pressed = GameState.sound_enabled
	
	$camera.make_current()
	for _i in range(10):
		$player.spawn_particle(true)

func _on_btn_start_pressed():
	if GameState.tutorial_done:
		Utils.load_level("res://scenes/levels/level_01.tscn")
	else:
		Utils.load_level("res://scenes/levels/tutorial.tscn")
	Utils.play_click()
	
func _on_btn_tutorial_pressed():
	Utils.load_level("res://scenes/levels/tutorial.tscn")
	Utils.play_click()
	
func _on_btn_levels_pressed():
	get_tree().change_scene_to(LevelMenu)
	Utils.play_click()

func _on_chk_sound_toggled(button_pressed):
	GameState.enable_sound(button_pressed)
	Utils.play_click()

func _on_easteregg_pressed():
	var n = $player.get_num_particles()
	if n <= 0:
		return
	
	var rnd = RandomNumberGenerator.new()
	rnd.randomize()

	var i = rnd.randi_range(0, n-1)
	var p = $player/particles.get_child(i)
	
	Utils.spawn_explosion(p.global_position)
	$player/sound/explosion.play()
	p.queue_free()
	
