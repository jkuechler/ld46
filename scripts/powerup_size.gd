extends Node2D
class_name Powerup

signal triggered

export (float,0,60) var duration: float = 5.0
export (float,-75,75) var swarm_size_delta: float = -25

var player: Player = null

func apply(p: Player):
	player = p
	player.particles.swarm_size += swarm_size_delta
	$timer.start(duration)
		
func reset():
	if not player:
		return
	player.particles.swarm_size -= swarm_size_delta
	
func disable():
	player = null

func _ready():
	$trigger.connect("body_entered", self, "_on_trigger_entered")

func _on_trigger_entered(body):
	emit_signal("triggered", self)
	$particles.emitting = true
	$tween.interpolate_property($sprite, 'modulate', Color.white, Color(1,1,1,0), 0.5)
	$tween.start()
	$sound.play()
	$trigger.disconnect("body_entered", self, "_on_trigger_entered")

func _on_timer_timeout():
	reset()
