extends KinematicBody2D

signal killed

var parent: Node2D = null
var velocity: Vector2 = Vector2()
var speed: float = 200.0

func reverse_direction(rand: bool = true):
	var ang = PI
	if rand:
		ang += rand_range(-PI/4.0, PI/4.0)
	velocity = velocity.rotated(ang)

func handle_collision(coll: KinematicCollision2D):
	var obj = coll.collider as Node
	
	if obj.is_in_group("walls") or obj.is_in_group('particles'):
		#velocity = velocity.bounce(coll.normal)
		reverse_direction(false)
	else:
		if obj.has_method('hit'):
			obj.hit()

		emit_signal("killed", self)
		queue_free()

func _ready():
	add_to_group('particles')
	parent = get_parent()
	set_random_velocity()
	
func set_random_velocity():
	velocity = Vector2(1, 0).rotated(rand_range(-PI, PI)) * speed

func _physics_process(delta):

	if position.distance_to(parent.position) > parent.swarm_size:
		# if we are outside the swarm size, go back ~towards the center
		velocity = (parent.position - position).normalized().rotated(rand_range(-PI/8.0, PI/8.0)) * speed
		
	elif (position + velocity*delta).distance_to(parent.position) > parent.swarm_size:
		# if we would go outside the swarm size, go back
		reverse_direction()
			
	var coll = move_and_collide(velocity * delta)
	if coll:
		handle_collision(coll)
		
