extends Node

const Game = preload("res://scenes/game.tscn")
const Explosion = preload("res://scenes/explosion.tscn")

var _sound_click: AudioStreamPlayer = null

func _ready():
	_sound_click = AudioStreamPlayer.new()
	_sound_click.stream = preload("res://assets/sfx/bip2.wav")
	_sound_click.volume_db = -10
	add_child(_sound_click)

func play_click():
	_sound_click.play()

func spawn_explosion(global_pos: Vector2, color: Color = Color(0.2, 0.2, 0.2)):
	var ex = Explosion.instance()
	ex.modulate = color
	ex.global_position = global_pos
	get_tree().get_root().add_child(ex)

func load_level(path: String) -> void:
	GameState.level_path = path
	get_tree().change_scene_to(Game)
