extends Node2D

signal triggered

export (int,0,5) var num_particles: int = 2
export (int,0,15) var max_particles: int = 11

func apply(p: Player):
	var n = num_particles
	var missing = max_particles - p.particles.get_child_count()

	if missing >= 0 and n > missing:
		n = missing
		
	p.call_deferred("spawn_num_particles", n)
		
func disable():
	pass
	
# Called when the node enters the scene tree for the first time.
func _ready():
	$trigger.connect("body_entered", self, "_on_trigger_entered")

func _on_trigger_entered(_body):
	$sprite.hide()
	emit_signal("triggered", self)
	$trigger.disconnect("body_entered", self, "_on_trigger_entered")
