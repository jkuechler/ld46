extends Node2D
class_name Level

export (String, FILE) var next_level: String
export (String) var level_name: String = ""

export (int,0,10) var min_swarm_star1: int = 0
export (int,0,10) var min_swarm_star2: int = 5
export (int,0,10) var min_swarm_star3: int = 8

export (int,0,20) var start_swarm_size: int = 10

var in_game_environment: bool = false

func _ready():
	# for quicker debugging, if the level is started as the 
	# active scene, switch to the game scene
	if get_tree().current_scene == self:	
		GameState.level_path = self.filename
		get_tree().change_scene_to(load("res://scenes/game.tscn"))
	else:
		in_game_environment = true

func get_num_stars(num_particles: int) -> int:
	if num_particles >= min_swarm_star3:
		return 3
	if num_particles >= min_swarm_star2:
		return 2
	if num_particles >= min_swarm_star1:
		return 1
	return 0
	
