extends Node2D

signal triggered

export (float,0,60) var duration: float = 5.0
export (float,-200,600) var speed_delta: float = +100.0

var player: Player = null

func apply(p: Player):
	player = p
	player.hspeed += speed_delta
	$timer.start(duration)
		
func reset():
	if not player:
		return
	player.hspeed -= speed_delta
	
func disable():
	player = null
	
# Called when the node enters the scene tree for the first time.
func _ready():
	$trigger.connect("body_entered", self, "_on_trigger_entered")

func _on_trigger_entered(_body):
	$sound.play()
	$particles_left.emitting = true
	$particles_right.emitting = true
	emit_signal("triggered", self)
	$trigger.disconnect("body_entered", self, "_on_trigger_entered")

func _on_timer_timeout():
	reset()
