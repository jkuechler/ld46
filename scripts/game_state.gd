extends Node

const GameStateSavePath = "user://gamestate.json"

var width: float = 600.0

var level_path: String = "res://scenes/levels/level_1.tscn"

var debug: bool = false

# saved state
var tutorial_done: bool = false
var sound_enabled: bool = true
var max_stars = {}

func _ready():
	print("GameState ready")
	load_gamestate()

func finished_level(filename: String, num: int) -> void:
	if not max_stars.has(filename) or num > max_stars[filename]:
		max_stars[filename] = num
		
	save_gamestate()
	
func get_max_stars(filename: String) -> int:
	if max_stars.has(filename):
		return max_stars[filename]
	
	return 0
	
func enable_sound(enable: bool) -> void:
	sound_enabled = enable
	AudioServer.set_bus_mute(0, not sound_enabled)
	save_gamestate()

func save_gamestate():
	var f = File.new()
	f.open(GameStateSavePath, File.WRITE)
	
	var state = {}
	state['tutorial-done'] = tutorial_done
	state['stars'] = max_stars
	state['sound-enabled'] = sound_enabled
	
	var json = JSON.print(state)
	f.store_string(json)
	f.close()
	
	print("State saved to ", GameStateSavePath)

func load_gamestate():
	var f = File.new()
	if f.file_exists(GameStateSavePath):
		f.open(GameStateSavePath, File.READ)
		var json = f.get_as_text()
		f.close()
		
		var res = JSON.parse(json)
		if res.error == OK:
			var state = res.result
			if state.has('tutorial-done'):
				tutorial_done = tutorial_done or state['tutorial-done']
			
			if state.has('sound-enabled'):
				sound_enabled = state['sound-enabled']
			
			if state.has("stars"):
				var loaded_max_stars = state['stars']
				for filename in loaded_max_stars.keys():
					var num_stars = loaded_max_stars[filename]
					finished_level(filename, num_stars)
		
		print("Game state loaded")
		print(max_stars)
	else:
		print("Game state file found")
