extends StaticBody2D

export (float,0,1000) var speed: float = 100

var direction = 1
onready var shape = $shape

func _physics_process(delta):
	position.x += speed*direction*delta
	
	if position.x - shape.shape.extents.x < 0:
		direction = 1
	if position.x + shape.shape.extents.x > 600:
		direction = -1
