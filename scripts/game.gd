extends Node2D

onready var player = $player
onready var time_start = OS.get_unix_time()

const SPEED: float = 200.0

func end_game():
	$fallback_camera.position.x = 300
	$fallback_camera.position.y = player.position.y
	$fallback_camera.make_current()
	
	for p in get_tree().get_nodes_in_group("powerups"):
		p.disable()
		p.queue_free()
	
	player.queue_free()
	player = null
	
	$ui/game_end.show()
	
	print(OS.get_unix_time() - time_start, " seconds")
	
func win_game():
	$ui/game_end/message.text = "Level finished!"
	$ui/game_end/message.add_color_override("font_color", Color.green)
	
	if has_next_level():
		$ui/game_end/btn_next.show()
		
	var swarm_size = $player/particles.get_child_count()
	var stars = $level.get_num_stars(swarm_size)
	if stars < 3:
		$ui/game_end/stars/star3.texture = preload("res://assets/nostar.png")
	if stars < 2:
		$ui/game_end/stars/star2.texture = preload("res://assets/nostar.png")
	if stars < 1:
		$ui/game_end/stars/star1.texture = preload("res://assets/nostar.png")
	
	$ui/game_end/stars.show()
	GameState.finished_level($level.filename, stars)
	end_game()

func lose_game():
	$ui/game_end/message.text = "Level lost!"
	$ui/game_end/message.add_color_override("font_color", Color.red)
	
	$ui/game_end/stars.hide()
	
	Utils.spawn_explosion($player.global_position, Color.gold)
	
	end_game()
	
func has_next_level():
	return has_node("level") and $level.next_level != ""
	
func _ready():
	player.position = Vector2(300, 1000)
	player.vspeed = SPEED
	
	var level: Node2D = load(GameState.level_path).instance()
	level.name = 'level'
	level.z_index = -1
	$ui/panel/level.text = level.level_name
	OS.set_window_title('Swarm: ' + level.level_name)
	
	add_child(level)

	player.spawn_num_particles(level.start_swarm_size)
	
	$ui/game_end.hide()

	$level/target.connect("body_entered", self, '_on_target_entered')
	
	for p in get_tree().get_nodes_in_group("powerups"):
		p.connect('triggered', self, '_on_powerup_triggered')

func _physics_process(_delta):
	if player:
		$left_wall.position.y = player.position.y - 400
		$right_wall.position.y = player.position.y - 400
	
		$ui/panel/swarm_size.text = 'Swarm size: ' + str($player/particles.get_child_count())
		var player_size = 10
		$ui/panel/distance.text = str(int(abs($level/target.position.y - $player.position.y) - player_size)) + ' m'
		
func _on_target_entered(body: PhysicsBody2D):
	if body == player:
		$sound/win.play()
		win_game()
		
func _on_powerup_triggered(powerup):
	powerup.apply(player)
		
func _on_player_collision(_with: Object):
	$sound/explosion.play()
	lose_game()

func _on_btn_menu_pressed():
	get_tree().change_scene("res://scenes/main_menu.tscn")


func _on_btn_next_pressed():
	if has_next_level():
		GameState.level_path = $level.next_level
		get_tree().change_scene(self.filename)

func _on_btn_retry_pressed():
	get_tree().change_scene(self.filename)
