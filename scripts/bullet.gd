extends KinematicBody2D

export (float,0,1000) var speed: float = 100.0
export (float,-1.57,1.57) var angle: float = 0.0

var velocity: Vector2

func hit():
	queue_free()

func _ready():
	velocity = Vector2(0, 1).rotated(-angle)
	
func _physics_process(delta):
	move_and_collide(velocity * speed * delta)

