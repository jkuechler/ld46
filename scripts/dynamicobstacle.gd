tool
extends Node2D

export (float,10,1000) var width: float = 600 setget _set_width
export (float,10,1000) var height: float = 50 setget _set_height

export (bool) var moving_horizontal: bool = false
export (float,0,1000) var horizontal_speed: float = 100
export (float,0,800) var horizontal_range: float = 200
export (int,-1,1) var horizontal_direction: int = 1
export (float,-800,800) var horizontal_offset: float = 0

export (bool) var moving_vertical: bool = false
export (float,0,1000) var vertical_speed: float = 50
export (float,0,800) var vertical_range: float = 200
export (int,-1,1) var vertical_direction: int = 1

export (bool) var rotating: bool = false
export (float,-10,10) var rotation_speed: float = 1.0

var verdir = 1
var hordir = 1

func _ready():
	update_size()
	
	if $handle.has_node("body"):
		var body = $handle/body
		$handle.remove_child(body)
		body.queue_free()
	
	var shape = RectangleShape2D.new()
	shape.extents = $handle/panel.rect_size / 2 - Vector2(1.5, 1.5)
	
	var cs = CollisionShape2D.new()
	cs.name = "shape"
	cs.set_shape(shape)
	
	var body = StaticBody2D.new()
	body.name = "body"
	$handle.add_child(body)
	body.add_child(cs)
	
	$handle.position = Vector2(0,0)
	$handle.rotation = 0
	
	if horizontal_direction != 0:
		hordir = horizontal_direction/abs(horizontal_direction)
	
	if vertical_direction != 0:
		verdir = vertical_direction/abs(vertical_direction)
		
	$handle.position.x = horizontal_offset

func _physics_process(delta):
	var h = $handle
			
	if moving_horizontal:
		h.position.x += horizontal_speed*hordir*delta
		
		if h.position.x > horizontal_range:
			hordir = -1
		elif h.position.x < -horizontal_range:
			hordir = 1
	else:
		h.position.x = 0
		
	if moving_vertical:
		h.position.y += vertical_speed*verdir*delta
		
		if h.position.y > vertical_range:
			verdir = -1
		elif h.position.y < -vertical_range:
			verdir = 1
	else:
		h.position.y = 0
	
	if rotating:
		h.rotation += (rotation_speed * PI * delta)
	
func update_size():
	if not has_node("handle"):
		return
		
	$handle/panel.rect_size.x = width
	$handle/panel.rect_size.y = height
	
	$handle/panel.rect_position.x = -width/2
	$handle/panel.rect_position.y = -height/2
	
	if $handle.has_node("body") and $handle/body.has_node("shape"):
		$handle/body/shape.shape.extents = $handle/panel.rect_size / 2 - Vector2(1.5, 1.5)
	
func _set_width(w: float):
	width = w
	update_size()
	
func _set_height(h: float):
	height = h
	update_size()
