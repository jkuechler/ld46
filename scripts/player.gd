extends KinematicBody2D
class_name Player

signal collision
signal moved

const Particle = preload("res://scenes/particle.tscn")

var hspeed: float = 200
var vspeed: float = 200

const WIDTH = 30

export (bool) var passive: bool = false

onready var particles = $particles

var particles_to_spawn = 0

func spawn_particle(silent: bool = false):
	var p = Particle.instance()
	p.connect("killed", self, "_particle_killed")
	particles.add_child(p)
	if not silent:
		$sound/spawn.play()
		
func spawn_num_particles(n: int):
	if n > 0:
		spawn_particle()
		$tween.interpolate_callback(self, 0.2, 'spawn_num_particles', n-1)
		$tween.start()
	
func get_num_particles() -> int:
	return particles.get_child_count()

func get_input() -> Vector2:
	var velocity = Vector2()
	
	# FIXME: hardcoded keys, because action map is not recognized in HTML
	# export.
	if Input.is_action_pressed("left") or Input.is_key_pressed(KEY_A):
		velocity.x += -1
	if Input.is_action_pressed("right") or Input.is_key_pressed(KEY_D):
		velocity.x += 1
		
	if Input.is_mouse_button_pressed(BUTTON_LEFT):
		var mouse = get_global_mouse_position()
		if mouse.x > position.x + 2:
			velocity.x = 1
		elif mouse.x < position.x - 2:
			velocity.x = -1
	
	velocity.y = -1
		
	return velocity
	
func handle_collision(coll: KinematicCollision2D):
	if not coll:
		return
		
	emit_signal("collision", coll.collider)
	
func _ready():
	randomize()
	
	if passive:
		$camera.current = false
	
func _draw():
	if GameState.debug:
		draw_circle(Vector2(0,0), particles.swarm_size, Color(0, 0, 0.5, 0.5))

func _physics_process(delta):
	if passive:	
		return
		
	var v = get_input()
	
	v.x *= hspeed
	v.y *= vspeed
	
	var newpos: Vector2 = position + v*delta

	if newpos.x < WIDTH/2.0 or newpos.x > (GameState.width - WIDTH/2.0):
		v.x = 0
	
	var coll = move_and_collide(v * delta)
	if coll:
		handle_collision(coll)
	
	if v.x != 0:
		emit_signal("moved", v.x > 0)

func _particle_killed(particle):
	Utils.spawn_explosion(particle.global_position)
	$sound/explosion.play()

