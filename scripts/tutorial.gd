extends Level

var player
var game

var state = 0

var has_moved_left = false
var has_moved_right = false

func set_state(next_state: int):
	state = next_state
	
	if state == 0:
		$ui/message.text = 'Welcome to SWARM!'
		$timer.start(2)
	elif state == 1:
		$ui/message.text = "Move left and right by clicking/touching, or using the 'A'/'D' or arrow keys."
	elif state == 2:
		$ui/message.text = "Good!"
		$timer.start(2)
	elif state == 3:
		$ui/message.text = "You will move forward automatically."
		$tween.interpolate_property(player, "vspeed", 0, 200, 2.0)
		$tween.interpolate_property($ui/message, 'modulate', Color.white, Color(1.0, 1.0, 1.0, 0.0), 2.5)
		$tween.interpolate_callback($snd_speed, 0.25, 'play')
		$tween.start()
		
		$timer.start(3)
	elif state == 4:
		$tween.stop_all()
		$ui/message.modulate = Color.white
		$ui/message.text = "Don't touch the obstacles!"
		
	elif state == 5:
		$ui/message.text = 'Of course you are not alone...'
		$tween.stop_all()
		$tween.interpolate_callback(self, 0.2, '_spawn_particles')
		$tween.start()
	
	elif state == 6:
		$ui/message.text = 'If your swarm collides with obstacles, they explode.\nBetter watch out!'
		$timer.start(3)
		
	elif state == 7:
		$ui/message.text = 'Keep your swarm alive and bring them all to the target.'
		$timer.start(5)
		
	elif state == 8:
		$ui/message.text = 'Good luck!'
		$tween.interpolate_property($ui/message, 'modulate', Color.white, Color(1.0, 1.0, 1.0, 0.0), 2.5)
		$tween.start()
		
		$timer.start(3)
	
	elif state == 9:
		pass
	
func _ready():
	._ready()
	
	
	
	if not in_game_environment:
		return
	
	game = get_parent()
	player = game.get_node("player")
	
	player.vspeed = 0
	player.connect("moved", self, '_on_player_moved')
	
	for p in player.particles.get_children():
		player.particles.remove_child(p)
		p.queue_free()
	
	set_state(0)
	
	GameState.tutorial_done = true
	
func _process(delta):
	if state == 1:
		if has_moved_left and has_moved_right:
			set_state(2)

func _on_player_moved(moved_right):
	print("MOVED", moved_right)
	if moved_right:
		has_moved_right = true
	else:
		has_moved_left = true
		
func _spawn_particles():
	print("PLOPP")
	var n = player.particles.get_child_count()
	if n < 10:
		player.spawn_particle()
		$tween.interpolate_callback(self, 0.2, '_spawn_particles')
		$tween.start()
	else:
		_trigger_next_state()

func _trigger_next_state():
	set_state(state+1)

func _on_area_2d_body_entered(body):
	_trigger_next_state()
