extends Control

const LevelPath = "res://scenes/levels/"

# Called when the node enters the scene tree for the first time.
func _ready():
	fill_level_list()

func get_level_files() -> Array:
	var dir = Directory.new()
	
	var levels = []
	
	if dir.open(LevelPath) == OK:
		dir.list_dir_begin()
		var fn = dir.get_next()
		while fn != "":
			print(fn)
			if fn.get_extension() == "tscn" and fn.begins_with("level_"):
				levels.append(LevelPath + fn)
			fn = dir.get_next()
	else:
		pass
	
	return levels
	
func get_level_name(path: String) -> String:
	var sc = load(path).instance()
	return sc.level_name
	
func fill_level_list() -> void:
	var levels = get_level_files()
	print(levels)
	for l in levels:
		var name = get_level_name(l)
		var num = GameState.get_max_stars(l)
			
		print(name, " ", num)
		
		var btn = Button.new()
		btn.text = name
		btn.connect("pressed", self, "_load_level", [l])
		btn.add_color_override("font_color", Color( 0.0941176, 0.623529, 0, 1 ))
		
		$scroll/tree.add_child(btn)
		
		var hbox = HBoxContainer.new()
		for i in range(3):
			var star = TextureRect.new()
			if num > i:
				star.texture = preload("res://assets/star.png")
			else:
				star.texture = preload("res://assets/nostar.png")
			hbox.add_child(star)
		
		$scroll/tree.add_child(hbox)

func _load_level(level: String) -> void:
	Utils.play_click()
	Utils.load_level(level)


func _on_btn_main_menu_pressed():
	Utils.play_click()
	get_tree().change_scene("res://scenes/main_menu.tscn")
